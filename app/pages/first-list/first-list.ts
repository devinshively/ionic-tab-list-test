import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/*
  Generated class for the FirstListPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/first-list/first-list.html',
})
export class FirstListPage {

  data = [{Name: "One"},
           {Name: "Two"},
           {Name: "Three"},
           {Name: "Four"},
           {Name: "Five"},
           {Name: "Six"},
           {Name: "Seven"},
           {Name: "Eight"},
           {Name: "Nine"},
           {Name: "Ten"},
           {Name: "Eleven"},
           {Name: "Twelve"},
           {Name: "Thirteen"},
           {Name: "Fourteen"},
           {Name: "Fifteen"},];

  items: any[];

  constructor(private navCtrl: NavController) {
    this.items = this.data;
  }

  itemSelected(item) {
    console.log(item);
  }

  search(searchTerm) {
    let term = searchTerm.target.value.trim().toLowerCase();
    if (term == '' || term.length < 2) {
      this.items = this.data.slice()
    } else {
      this.items = this.data.slice().filter((item) =>
        item.Name.toLowerCase().includes(term)
      )
    }
  }

}
