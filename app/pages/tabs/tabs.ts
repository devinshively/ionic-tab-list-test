import {Component} from '@angular/core';
import {HomePage} from '../home/home';
import {AboutPage} from '../about/about';
import {ContactPage} from '../contact/contact';
import {FirstListPage} from '../first-list/first-list'
import {SecondListPage} from '../second-list/second-list'


@Component({
  templateUrl: 'build/pages/tabs/tabs.html'
})
export class TabsPage {

  private firstList: any;
  private secondList: any;
  private tab1Root: any;
  private tab2Root: any;
  private tab3Root: any;

  constructor() {
    // this tells the tabs component which Pages
    // should be each tab's root Page
    this.firstList = FirstListPage;
    this.secondList = SecondListPage;
    this.tab1Root = HomePage;
    this.tab2Root = AboutPage;
    this.tab3Root = ContactPage;
  }
}
